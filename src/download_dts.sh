pushd wdir/sources/linux-5.13.12/arch/arm64/boot/dts/broadcom/
BASE="https://raw.githubusercontent.com/raspberrypi/linux/rpi-5.10.y/arch/arm/boot/dts/"
wget "${BASE}bcm2710.dtsi"                    -O bcm2710.dtsi
wget "${BASE}bcm2709-rpi.dtsi"                -O bcm2709-rpi.dtsi
wget "${BASE}bcm283x-rpi-smsc9514.dtsi"       -O bcm283x-rpi-smsc9514.dtsi
wget "${BASE}bcm283x-rpi-csi1-2lane.dtsi"     -O bcm283x-rpi-csi1-2lane.dtsi
wget "${BASE}bcm283x-rpi-i2c0mux_0_44.dtsi"   -O bcm283x-rpi-i2c0mux_0_44.dtsi
wget "${BASE}bcm271x-rpi-bt.dtsi"             -O bcm271x-rpi-bt.dtsi
wget "${BASE}bcm283x-rpi-cam1-regulator.dtsi" -O bcm283x-rpi-cam1-regulator.dtsi

wget "${BASE}bcm2837.dtsi"                    -O bcm2837.dtsi
wget "${BASE}bcm270x.dtsi"                    -O bcm270x.dtsi
wget "${BASE}bcm2708-rpi.dtsi"                -O bcm2708-rpi.dtsi
wget "${BASE}bcm2835-rpi.dtsi"                -O bcm2835-rpi.dtsi
wget "${BASE}bcm270x-rpi.dtsi"                -O bcm270x-rpi.dtsi

wget "${BASE}bcm283x.dtsi"                    -O bcm283x.dtsi
wget "${BASE}bcm2835-common.dtsi"             -O bcm2835-common.dtsi
wget "${BASE}bcm2835-rpi-common.dtsi"         -O bcm2835-rpi-common.dtsi
wget "${BASE}bcm2835-rpi.dtsi"                -O bcm2835-rpi.dtsi
wget "${BASE}bcm270x-rpi.dtsi"                -O bcm270x-rpi.dtsi
popd
