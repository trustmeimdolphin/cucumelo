#!/bin/sh

echo "Creating building directory"
mkdir build
echo "Creating image cucumelo.img"
truncate -s 1886M ${IMAGE}

echo "Creating partitions"
#echo "g
#n
#1
#
#+200M
#n
#2
#
#
#t
#1
#1
#w
#" | fdisk ${IMAGE}
echo "o
n
p
1

+256M
n
p
2


t
1
0b
w
" | fdisk ${IMAGE}

echo "Setting up the device"
export LOOP=$(sudo losetup --partscan --show --find ${IMAGE})
echo "Setup on ${LOOP}"
sudo mkfs.vfat -n boot ${LOOP}p1
sudo mkfs.ext4 -L root ${LOOP}p2

mkdir -p ${TGTFS}
sudo mount ${LOOP}p2 ${TGTFS}
sudo chmod -R 777 ${TGTFS}
mkdir -p ${TGTFS}/boot
sudo mount ${LOOP}p1 ${TGTFS}/boot
sudo chmod -R 777 ${TGTFS}

read -n 1 -p "Lalalalala"
#sudo losetup -d ${LOOP}
