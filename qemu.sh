#!/bin/sh
qemu-img convert -f raw -O qcow2 build/cucumelo.img cucumelo.qcow
qemu-img resize cucumelo.qcow 2G
qemu-system-aarch64 -M raspi3 -append "rw earlyprintk loglevel=8 console=ttyAMA0,115200 console=ttyS0,115200 console=ttyS1,115200 dwc_otg.lpm_enable=0 root=/dev/mmcblk0p2 rootdelay=1" -sd cucumelo.qcow -kernel wdir/cucumelo-5.13.12 -dtb ./wdir/bcm2837-rpi-3-b.dtb -m 1G -smp 4 -serial vc -serial vc -serial vc -usb -device usb-mouse -device usb-kbd
