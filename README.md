# Introduction

Cucumelo is a minimal linux distro for raspberry pi based on LFS

## Easy building

./run.sh

### Real steps

1. Create the disk image for building:
    src/builder.sh
1. Get the dependencies for building the target system:
    src/dependencies.sh
1. Verify (If possible) the signatures of the downloaded dependencies:
    src/verify_dependencies.sh
1. Create the toolchain for cross-compiling the target system:
    src/toolchain.sh
1. Build the target system's directory structure
    src/target_fs.sh
1. Create initial files for the target system and basic packages (i.e. libc and busybox)
    src/initial_files.sh
1. Build the kernel and the bootloader installing them on the `/boot` partition:
    src/boot.sh
1. Add configuration for the services that run when the kernel boots
    src/boot_scripts.sh
1. Copy the Cucumelo banner
    src/copy_banner.sh
1. Set the kernel config, raspberry's config.txt and u-boot configuration
    src/configs.sh
1. Install the raspberry's bootcode for 1st stage bootloader (WIP)
    src/bloatcode.sh
1. Edit the filesystem permissions
    src/edit_permissions.sh



### Toolchain building

In order to create a toolchain for cross-compiling we need:

1. Install linux headers
1. Create our initial compiler for our host machine that is gonna build the C library for out target machine (GCC)
1. Compile a libc implementation for the target machine (musl)
1. Create the final compiler (GCC)


## Testing on qemu

For easy testing on qemu there is a `qemu.sh` script that will run all the commands necessary to run the already built image on qemu

## Tested boards and architectures:

* Raspberry Pi 3B: arm64
