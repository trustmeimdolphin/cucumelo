#!/bin/sh

sudo umount wdir/lfs/boot
sudo umount wdir/lfs
sudo losetup -d /dev/loop0
find wdir/sources -type d -maxdepth 1 -mindepth 1 -exec rm -fR {} \+
